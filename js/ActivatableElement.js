export default class ActivatableElement {

	static _debug = 0;
	static cssClass = "activatable_element";

	_defaults = {
		finalized_class: false // set finalized class (because that would mean element is not really in pre-init state)
	}
	_element
	_elements
	_state
	_options
	_callbacks // custom events (like ActivatableState change)
	_events // native js events
	_cssClass
	_registeredChildren
	__registeredChildren // for use of children registered with public method (exclude from being resetted)
	_queuedChildren // added with addChildren
	isRegistered // is the state managed by a parent?

	constructor(element, options) {
		if (!(element instanceof(Element))) {
			console.error("Invalid agument", element, options);
		}
		this._options = {...this._defaults, ...options, ...element.dataset};
		// inheriting classes with defaults need this:
		// this._options = {...this._defaults, ...this._options};

		this._element = element;
		this._elements = {};
		this._registeredChildren = {};
		this.__registeredChildren = {};
		this._queuedChildren = [];
		this._callbacks = {};
		this._events = {};

		this._createBindings();

		if (typeof this._options.callbacks == "object") {
			for (let prop in this._options.callbacks) {
				this.on(prop, this._options.callbacks[prop]);
			}
		}
		if (typeof this._options.events == "object") {
			for (let prop in this._options.events) {
				this.on(prop, this._options.events[prop], true); // for custom element, use on() method directly
			}
		}

		this.on(ActivatableState.INITIALIZED, () => this._onInitialized());
		this.on(ActivatableState.ACTIVATED,   () => this._onActivated());
		this.on(ActivatableState.DEACTIVATED, () => this._onDeactivated());
		this.on(ActivatableState.FINALIZED,   () => this._onFinalized());

		this.on("before_"+ActivatableState.INITIALIZING, () => this._onBeforeInitialize());
		this.on("before_"+ActivatableState.ACTIVATING,   () => this._onBeforeActivate());
		this.on("before_"+ActivatableState.DEACTIVATING, () => this._onBeforeDeactivate());
		this.on("before_"+ActivatableState.FINALIZING,   () => this._onBeforeFinalize());

		// TODO: evaluate if this is child or parent of any other Activatable Element
		document.dispatchEvent(new CustomEvent('module_constructed', {
			detail: this
		}));
	}

	async init() {
		this.log("init");
		if (this.state == ActivatableState.ACTIVATED) {
			// reinitialize
			await this.__deactivate();
			await this.__finalize();
			await this.__initialize();
			await this.__activate();
		} else {
			await this.__initialize();
			await this.__activate();
		}
	}

	async activate() {
		if (this.state !== ActivatableState.ACTIVATED) {
			await this.__activate();
		}
	}

	async deactivate() {
		if (this.state !== ActivatableState.DEACTIVATED) {
			await this.__deactivate();
		}
	}

	async destroy() {
		if (this.state !== ActivatableState.DEACTIVATED) {
			await this.__deactivate();
		}
		await this.__finalize();
	}

	registerChild(activatableElement, label) {
		this.__registeredChildren[label || Object.keys(this.__registeredChildren).length] = activatableElement;
		this._registerChild(activatableElement, label);
	}

	_registerChild(activatableElement, label) {
		activatableElement.isRegistered = true;
		this._registeredChildren[label || Object.keys(this._registeredChildren).length] = activatableElement;
	}

	_addChild(args) {
		console.warn("_addChild is deprecated, use queueChild");
		this._queueChild(...args);
	}

	_queueChild(classProp, element, options, label, callbackAfterConstructed){
		this._queuedChildren.push({
			class: classProp,
			element: element,
			options: options || {},
			label: label,
			callback: callbackAfterConstructed
		});
	}

	_registerQueuedChildren(){
		for(let child of this._queuedChildren){
			// handle element (string or dom element)

			if(typeof child.element == 'string') {
				child.element = this._element.querySelector(child.element);
			}
			if(typeof child.element == "object") {
				this._elements[Object.keys(child.element)[0]] = Object.values(child.element)[0];
			}

			let instance = new child.class(child.element, child.options);
			this._registerChild(instance, child.label);

			if(child.callback) {
				child.callback.call(this, instance);
			}
		}
	}

	_registerElements(elements){
		if (elements) {
			for(let element_name in elements) {
				this._elements[element_name] = elements[element_name].element;
			}
		}
	}

	// create scope bound event handlers so we can use "this" inside to get this instance
	_createBindings() {
		this.__onRefresh = this._onRefresh.bind(this);
	}

	_onBeforeInitialize() {
		if (this.state && (this.state !== ActivatableState.FINALIZED && this.state !== ActivatableState.INITIALIZING)) {
			throw `cannot set state ${ActivatableState.INITIALIZING} from ${this.state}`;
		}
		if (this._element.classList.contains(ActivatableState.INITIALIZED)) {
			throw `element seems to be initialized already `+ this.constructor.cssClass;
		}

		// Check if module is constructed already
		if(this._element.dataset.module){
			throw "Already constructed";
		}
		// Add module indicator
		this._element.dataset.module = this.constructor.name;

		this._registerElements();
		this._registerChildren();
	}

	_onInitialized() {

	}

	_onBeforeActivate() {
		if (this.state !== ActivatableState.ACTIVATING && this.state !== ActivatableState.INITIALIZED && this.state !== ActivatableState.DEACTIVATED) {
			throw `cannot set state ${ActivatableState.ACTIVATING} from ${this.state}`;
		}
	}

	_onActivated() {

	}

	_onBeforeDeactivate() {
		if (this.state !== ActivatableState.DEACTIVATING && this.state !== ActivatableState.ACTIVATED) {
			throw `cannot set state ${ActivatableState.DEACTIVATING} from ${this.state}`;
		}
	}

	_onDeactivated() {

	}

	_onBeforeFinalize() {
		if (this.state !== ActivatableState.FINALIZING && this.state !== ActivatableState.DEACTIVATED) {
			throw `cannot set state ${ActivatableState.FINALIZING} from ${this.state}`;
		}
	}

	_onFinalized() {
		this._element.removeAttribute("data-module");
		this._registeredChildren = {...this.__registeredChildren}; // reset this to publicly registered children
	}

	// get data, build dom
	async __initialize() {
		this.state = ActivatableState.INITIALIZING;

		await this._initialize();

		this._hasCssClass = this._element.classList.contains(this.constructor.cssClass);
		if (!this._hasCssClass) {
			this._element.classList.add(this.constructor.cssClass);
		}

		if (Object.keys(this._registeredChildren).length) {
			await Promise.all(Object.values(this._registeredChildren).map(child => child.__initialize()));
		}
		this.state = ActivatableState.INITIALIZED;
	}

	async _initialize() {

	}

	// enable interactivity
	async __activate() {
		this.state = ActivatableState.ACTIVATING;

		await this._activate();

		for (let type in this._events) {
			var map = this._events[type];
			map.forEach((listeners, element, map) => {
				for (let listener of listeners) {
					element.addEventListener(type, listener);
				}
			});
		}
		this.on("refresh", this.__onRefresh);

		if (Object.keys(this._registeredChildren).length) {
			await Promise.all(Object.values(this._registeredChildren).map(child => child.__activate()));
		}
		this.state = ActivatableState.ACTIVATED;
	}

	async _activate() {

	}

	// disable interactivity
	async __deactivate() {
		this.state = ActivatableState.DEACTIVATING;

		await this._deactivate();

		for (let type in this._events) {
			var map = this._events[type];
			map.forEach((listeners, element, map) => {
				for (let listener of listeners) {
					element.removeEventListener(type, listener);
				}
			});
		}
		this.off("refresh", this.__onRefresh);

		if (Object.keys(this._registeredChildren).length) {
			await Promise.all(Object.values(this._registeredChildren).map(child => {
				return child.__deactivate()
			}));
		}
		this.state = ActivatableState.DEACTIVATED;
	}

	async _deactivate() {

	}

	// restore clean state (pre initialization)
	async __finalize() {
		this.state = ActivatableState.FINALIZING;

		await this._finalize();

		// remove only if it did not have the class initially
		if (!this._hasCssClass) {
			this._element.classList.remove(this.constructor.cssClass);
		}

		if (Object.keys(this._registeredChildren).length) {
			await Promise.all(Object.values(this._registeredChildren).map(child => child.__finalize()));
		}
		this.state = ActivatableState.FINALIZED;
	}

	async _finalize() {

	}

	_registerChildren() {
		this._registerQueuedChildren();
	}

	_registerElements() {

	}

	_getRegisteredChild(id) {
		return this._registeredChildren[id];
	}

	_addCallback(type, fn) {
		if (!this._callbacks[type]) {
			this._callbacks[type] = [];
		}
		this._callbacks[type].push(fn);
	}

	_addEventlistener(type, fn, element) {
		if (!this._events[type]) {
			this._events[type] = new Map();
		}
		const listeners = this._events[type].get(element) || [];
		listeners.push(fn);
		this._events[type].set(element, listeners);
	}

	_removeEventlistener(type, fn, element) {
		const listeners = this._events[type].get(element);
		this._events[type].set(element, listeners.filter(stored => fn != stored));
		element.removeEventListener(type, fn);
	}

	_removeCallback(type, fn) {
		if (!fn) {
			this._callbacks[type] = [];
		} else if (this._callbacks[type]) {
			this._callbacks[type] = this._callbacks[type].filter(stored => fn != stored)
		}
	}

	_fireEvent(type, args) {
		if (this._callbacks[type]) {
			for (let fn of this._callbacks[type]) {
				this.log("_fireEvent callback", type, fn, this._element);
				fn.apply(this, args);
			}
		}
	}

	async refresh() {
		this.refreshing = true;
		await this.init();
		this._fireEvent("refresh");
	}

	_onRefresh() {
		this.log("_onRefresh");
		this.refreshing = false;
	}

	get element() {
		return this._element;
	}

	set element(element) {
		this._element = element;
	}

	get state() {
		return this._state;
	}

	set state(state) {
		if (this._state != state) {
			this._fireEvent("before_"+state);
		}
		this._element.classList.remove(this._state);
		if (state !== ActivatableState.FINALIZED || this._options.finalized_class) {
			this._element.classList.add(state);
		}
		this.log(`set state ${this._state} to ${state}`, this._element);
		this._state = state;

		this._fireEvent(state);
	}

	on(type, fn, element) {
		if (element) {
			if (!(element instanceof Element)) {
				element = this._element;
			}
			this._addEventlistener(type, fn, element);
		} else {
			this._addCallback(type, fn);
		}
	}

	off(type, fn, element) {
		if (element) {
			if (!(element instanceof Element)) {
				element = this._element;
			}
			this._removeEventlistener(type, fn, element);
		} else {
			this._removeCallback(type, fn);
		}
	}

	once(type, fn, native) {
		var cb = () => {
			this.off(type, cb, native);
			fn();
		}
		if (native) {
			this._addEventlistener(type, cb);
		} else {
			this._addCallback(type, cb);
		}
	}

	promise(type) {
		return new Promise(resolve => {
			this.once(type, resolve);
		});
	}

	log() {
		if (this.constructor._debug || this.debug) {
			console.groupCollapsed(...[this.constructor.name, ...arguments]);
			console.trace();
			console.groupEnd();
		}
	}
}

export class ActivatableState {
	static INITIALIZING = "initializing";
	static INITIALIZED  = "initialized";
	static ACTIVATING   = "activating";
	static ACTIVATED    = "activated";
	static DEACTIVATING = "deactivating";
	static DEACTIVATED  = "deactivated";
	static FINALIZING   = "finalizing";
	static FINALIZED    = "finalized";
}
