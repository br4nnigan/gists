import ActivatableElement, {ActivatableState} from "/modules/ActivatableElement.js";

export default class AjaxForm extends ActivatableElement {

	static _debug = 0
	static cssClass = "ajax_form";

	_valueMap
	_action
	_method

	_defaults = {
		"edit_live": false, // send on every input change
		"input_selector": "[name]",
		"xhr_settings": {},
		"send_all_on_live_edit": false, // send complete form on live element change? inputs with class live_edit_send are also always sent
		"form_data": {}, // always add this data
		"validate": true, // check required attributes
		"native_validate": true, // use form.checkValidity, bad idea if there are required inputs nested that are not to be used,
		"force_input_filtering": false, // use input filtering even if _options.inputs were given
		on_before_submit: () => {} // callback that can return promise (therefore not using activatable callback interface),
	}


	constructor(element, options) {
		super(element, options);
		this._options = {...this._defaults, ...this._options};

		this._action = element.getAttribute("action") || element.dataset.action;
		this._method = element.getAttribute("method") || element.dataset.method;

		this.log("new tag", this._options);
	}

	_initialize() {
		this.state = ActivatableState.INITIALIZING;

		if (typeof this._options.inputs == "function") {
			this._elements.inputs = this._options.inputs(this._element);
		} else {
			this._elements.inputs = this._options.inputs || Array.from(this._element.querySelectorAll(this._options.input_selector)).filter(el => this._filterInput(el));
		}
		this._elements.inputs = Array.from(this._elements.inputs); // ensure array
		if (this._options.force_input_filtering) {
			this._elements.inputs = this._elements.inputs.filter(el => this._filterInput(el));
		}
		this._valueMap = new Map();
		for (let el of this._elements.inputs) {
			this._valueMap.set(el, el.value);
		}

		this._elements.submitEl = (function(el) {
			if (el && el.closest(".ajax_form") == this._element && !el.closest(".ajax_live")) {
				return el;
			}
		}.bind(this))(this._element.querySelector("[type='submit']"));

		super._initialize();
	}

	_activate() {
		this.state = ActivatableState.ACTIVATING;

		for (let el of this._elements.inputs) {
			el.addEventListener("change", this.__onElementChange);
			el.addEventListener("input", this.__onElementInput);
		}
		if (this.element.tagName === "FORM") {
			this.element.addEventListener("submit", this.__onSubmit);
		}
		if (this._elements.submitEl) {
			this._elements.submitEl.addEventListener("click", this.__onSubmit);
		}
		super._activate();
	}

	_deactivate() {
		this.state = ActivatableState.DEACTIVATING;

		for (let el of this._elements.inputs) {
			el.removeEventListener("change", this.__onElementChange);
			el.addEventListener("input", this.__onElementInput);
		}
		if (this.element.tagName === "FORM") {
			this.element.removeEventListener("submit", this.__onSubmit);
		}
		if (this._elements.submitEl) {
			this._elements.submitEl.removeEventListener("click", this.__onSubmit);
		}
		super._deactivate();
	}

	_filterInput(element) {
		// filter out null values
		var exists = !!element;
		// filter out all sub form elements
		var is_in_subform = this._element != element.closest(".ajax_form") && this._element.contains(element.closest(".ajax_form"));
		// this.log('test', this._element, element.closest(".ajax_form"));
		// filter out ajax_live form elements
		var is_in_ajaxlive = element.closest(".ajax_live");
		// filter out explicitly ignored inputs
		var is_ignored = element.classList.contains("edit_live_ignore");
		this.log("_filterInput", exists, is_in_subform, is_in_ajaxlive, is_ignored, element)
		return exists && !is_in_subform && !is_in_ajaxlive && !is_ignored;
	}

	_createFormData(send_all) {
		const formData = new FormData();
		// append static form data?
		for (let prop in this._options.form_data) {
			formData.append(prop, this._options.form_data[prop]);
		}
		for (let el of this._elements.inputs) {
			if (send_all || el.classList.contains("live_edit_send")) {
				let n = el.getAttribute("name");
				n && formData.append(n, el.value);
			}
		}
		return formData;
	}

	_onElementInput(e) {
		var name = e.target.getAttribute("name");
		var value =  e.target.value;
		this.log("_onElementInput", name, value, e);

		if(e.target.classList.contains("prevent-invalid") && e.target.value && !this._validateElement(e.target)) {
			e.preventDefault();
			e.target.value = this._valueMap.get(e.target); // restore previous value
			return false;
		}
		this._valueMap.set(e.target, value); // update stored value

		if (this._options.edit_live || e.target.classList.contains("live_input")) {
			let formData = this._createFormData(this._options.send_all_on_live_edit);
				formData.set(name, value);
				formData.set("name", name);
				formData.set("value", value);

			this._fireEvent("element_live_input", [e, formData]);
			this._send(formData);
		} else {
			this._fireEvent("element_input", [e]);
		}
	}

	_onElementChange(e) {
		var name = e.target.getAttribute("name");
		var value =  e.target.value;
		this.log("_onElementChange", name, value, e.target);
		this._fireEvent("element_change", [e]);
	}

	_onSubmit(e) {
		e.preventDefault();
		this.submit();
	}

	_validateElement(el) {
		return !(el.required && (!el.checkValidity() || (el.getAttribute("type") == "hidden" && el.value === "")));
	}

	_getFailedValidations() {
		const failed_validations = this._elements.inputs.map(el => {
			if (!this._validateElement(e)) {
				return el.getAttribute("placeholder") || el.dataset.placeholder || el.getAttribute("name");
			}
		}).filter(e => e);
		this.log("_getFailedValidations", failed_validations, this._elements.inputs);
		return failed_validations;
	}

	_validate() {
		if (typeof this._options.validate == "function") {
			return this._options.validate(this._elements.inputs);
		}
		const failed_validations = this._getFailedValidations();
		this.log('_validate', this._options.native_validate,  this.element.tagName == "FORM");
		if (this._options.native_validate && this.element.tagName == "FORM") {
			return this.element.checkValidity() || failed_validations;
		} else {
			return failed_validations.length ? failed_validations : true;
		}
	}

	submit() {
		this.log("submit");
		let form_data = this._createFormData(true);

		if (this._options.validate) {
			let result = this._validate();
			if (result !== true) {
				this._fireEvent("validation_fail", [result]);
				return false;
			}
		}
		if (typeof this._options.on_before_submit == "function") {
			var result = this._options.on_before_submit.call(this, form_data)
			// can be async
			if (result instanceof Promise) {
				result.then(() => this._send(form_data));
				return;
			}
		}
		this._send(form_data);
	}

	async _send(form_data) {
		this.log("_send", this._action, this._method, Object.fromEntries(form_data));
		if (this._action && this._method) {
			const response = await fetch(this._action, {
				method: this._method,
				body: form_data
			});
			this.log("send response", response);
			if (response.ok) {
				this._fireEvent("success", [response]);
			} else {
				this._fireEvent("error", [response]);
			}
		}
	}

	_createBindings() {
		super._createBindings();

		this.__onSubmit        = this._onSubmit.bind(this);
		this.__onElementChange = this._onElementChange.bind(this);
		this.__onElementInput  = this._onElementInput.bind(this);
	}
}
