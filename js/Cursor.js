var Cursor = function (options) {

	var
	cursor = this,
	el, $el, $body, $documentElement, lastEvent, ie, edge, scale, positioning, myIframeNode, bodyOffset, windowWidth, windowHeight, position, positionScaled, elementBelow, lastElementBelow, elementBelowOffset, lastElementBelowY, lastElementBelowX, lastMouseoverElement, scroll,
	settings,
	defaults,

	init = function (options)
	{
		defaults = {
			x: 0,
			y: 0,
			duration: 1500,

			// callback functions
			step: false,
			onClick: false,
			onMousedown: false,
			onMouseup: false,
			onDoubleClick: false,
			complete: false,

			// events to trigger
			events: {
				// events can be boolean, string or array of strings
				mousedown: "mousedown",
				mousemove: [
					"mousemove",
					"hover",
					"mousemove.jqs",
					"mousemove-filtered",
					"onDocumentMouseMove",
					"onContainerMouseMove"
				],
				click: true,
				mouseup: true,
				dblclick: true,
				mouseover: true,
				mouseenter: true,
				mouseleave: true
			},
			disableEvents: false
		};

		settings = $.extend({}, defaults, options);

		// make all event options an array
		for (var event in settings.events)
		{
			if (settings.events.hasOwnProperty(event)) {
				settings.events[event] = getEventsArray(settings.events[event], event);
			}
		}

		ie                = /MSIE/.test(navigator.userAgent);
		edge              = /Edge/.test(navigator.userAgent);
		$body             = $(document.body);
		$documentElement  = $(document.documentElement);
		$el               = $("<div class='cursor' style='left: "+settings.x+"px; top:"+settings.y+"px; opacity: 0'></div>").appendTo( $body );
		el                = $el.get(0);
		lastElementBelowY = 0;
		lastElementBelowX = 0;
		myIframeNode      = getMyIframeNode(); // iframe dom element of this window

		$.extend(cursor, settings, $el);

		cursor.update();

		elementBelowOffset = $(elementBelow)._offset();

		return cursor.animate({
			opacity: 1
		}, {
			queue: false
		}); // return animate object so that jquery.delay() can be used on it!
	},

	triggerEvent = function( type, element, options )
	{
		var settings = $.extend( {
			nativeJs: false,
			appendEvent: false,
			trigger: true
		}, options ),
		evt;

		if ( typeof type == "string" && element !== null && typeof element == "object" && element.nodeType === 1 )
		{
			if ( settings.nativeJs )
			{
				evt = document.createEvent("HTMLEvents");
				evt.initEvent(type, false, true);
				evt.element = evt.srcElement = evt.toElement = element;
				setEventPosition(evt);

				if ( ["click", "mousedown", "mouseup", "dblclick"].indexOf(type) != -1 ) evt.which = 1;
				if ( settings.appendEvent ) evt.originalEvent = $.extend(evt.originalEvent,evt);

				if ( settings.trigger ) element.dispatchEvent(evt);
			}
			else
			{
				evt = jQuery.Event(type);
				evt.element = evt.srcElement = evt.toElement = element;
				setEventPosition(evt);

				if ( ["click", "mousedown", "mouseup", "dblclick"].indexOf(type) != -1 ) evt.which = 1;
				if ( settings.appendEvent ) evt.originalEvent = $.extend(evt.originalEvent,evt);

				if ( settings.trigger ) $(element).trigger(evt);
			}
		}
		else
		{
			console.error("invalid arguments");
		}

		// console.log("triggerEvent", arguments);
		return evt;

	},

	// formatting of events option
	getEventsArray = function (value, key) {

		if ( typeof value == "string" ) return [value];

		else if ( typeof value == "boolean" )
		{
			if ( value === true )
			{
				if ( typeof key == "string" )
				{
					return [key];
				}
				else
				{
					console.error("provided boolean but missing key argument");
				}
			}
		}

		else if ( typeof value == "object" && Array.isArray(value) ) return value;

		return [];
	},

	onCursorMove = function ()
	{

		var evt, $elementBelow, $lastElementBelow;

		if ( !settings.disableEvents && (settings.events.mouseenter.length || settings.events.mouseover.length || settings.events.mouseleave.length || settings.events.mousemove.length) )
		{

			lastElementBelow = elementBelow;

			cursor.update(false, true);

			$elementBelow     = $(elementBelow);
			$lastElementBelow = $(lastElementBelow);


			// mouseenter / mouseleave
			if ( $elementBelow.length + $lastElementBelow.length == 2 && elementBelow != lastElementBelow )
			{

				for (var i = 0; i < settings.events.mouseenter.length; i++) {
					evt = triggerEvent(settings.events.mouseenter[i], elementBelow, {nativeJs: true}); // IE 9+ stuff weil jquery nicht funzt
				}

				if ( $(lastElementBelow).find(elementBelow).length === 0 )
				{
					for (var j = 0; j < settings.events.mouseleave.length; j++) {
						evt = triggerEvent(settings.events.mouseleave[j], elementBelow);
					}
				}
			}

			// mousemove / mouseover
			if ( $elementBelow.length == 1 )
			{
				if ( evt || !elementBelowOffset )
				{
					elementBelowOffset = $elementBelow.offset(); // if mouseenter/mouseleave happened or first time
				}

				// mouseover
				if ( lastMouseoverElement != elementBelow )
				{
					for (var k = 0; k < settings.events.mouseover.length; k++) {
						evt = triggerEvent(settings.events.mouseover[k], elementBelow);
					}
					lastMouseoverElement = elementBelow;
				}

				// mousemove
				for (var l = 0; l < settings.events.mousemove.length; l++) {
					evt = triggerEvent(settings.events.mousemove[l], elementBelow);
				}
			}
		}

		lastEvent = evt;

		if (typeof settings.step == "function" )
		{
			settings.step.call(cursor,evt);
		}

	},

	onCursorComplete = function ()
	{
		requestAnimationFrame(function () {

			cursor.update();
			lastMouseoverElement = null;

			if (typeof settings.complete == "function")
			{
				settings.complete.call(cursor, lastEvent);
				settings.complete = false;
			}
		});
	},


	getMyIframeNode = function ()
	{
		if ( window == top )
		{
			return null; // window has no parent frame
		}

		for (var i = parent.document.getElementsByTagName("iframe").length - 1, iframe; i >= 0; i--)
		{
			iframe = parent.document.getElementsByTagName("iframe")[i];
			if (iframe.contentWindow.document == document) return iframe;
		}
	},


	getElementAtCurrentPoint = function ( lastPointTolerance )
	{
		// console.log("getElementAtCurrentPoint ");

		var
		x, y, // current point
		myIframeNodeOffset,
		parentElementBelow, // element of parent window at current point
		parentElementsBelow = []; // all elements of parent window at current point

		x = positionScaled.left - scroll.x;
		y = positionScaled.top - scroll.y - 1;

		if ( ie && lastPointTolerance && getPointDistance(cursor.x, cursor.y, lastElementBelowY, lastElementBelowX) < lastPointTolerance )
		{
			return elementBelow; // save processing time
		}

		lastElementBelowX = y;
		lastElementBelowY = x;

		elementBelow = document.elementFromPoint( x,y );


		// possible parent iframe element overlapping in IE? (elementFromPoint is considers all frames in IE)
		if ( (ie || edge) && myIframeNode !== null && elementBelow === null && $documentElement.is(":visible") )
		{
			myIframeNodeOffset = "_offset" in $.fn ? $(myIframeNode)._offset() : $(myIframeNode).offset();

			parentElementBelow = parent.document.elementFromPoint( x+myIframeNodeOffset.left,y+myIframeNodeOffset.top );

			while ( elementBelow === null && parentElementBelow !== null )
			{
				if ( parentElementBelow == myIframeNode || parentElementBelow.nodeType !== 1 || parentElementBelow.tagName == "HTML" || parentElementBelow.tagName == "BODY") {
					break;
				}

				parentElementsBelow[parentElementsBelow.length] = {
					el: parentElementBelow,
					display: parentElementBelow.style.display
				};

				parentElementBelow.style.display = "none";

				elementBelow       = document.elementFromPoint( x, y );
				parentElementBelow = parent.document.elementFromPoint( x+myIframeNodeOffset.left,y+myIframeNodeOffset.top );
			}

			for (var i = parentElementsBelow.length - 1; i >= 0; i--)
			{
				parentElementBelow = parentElementsBelow[i];
				parentElementBelow["el"].style.display = parentElementBelow["display"];
			}
		}

		return elementBelow;
	},

	getScale = function ()
	{
		return document.body.getBoundingClientRect().width / document.body.offsetWidth;
	},

	hasTransform = function ( el )
	{
		while ( el && getComputedStyle(el).transform == "none" )
		{
			el = el.parentElement
		}
		return el !== null;
	},

	setEventPosition = function( evt )
	{
		evt.simulated = true;

		evt.pageX = evt.clientX = position.left
		evt.pageY = evt.clientY = position.top


		if ( elementBelowOffset ) evt.offsetX = evt.layerX = (position.left - elementBelowOffset.left);
		if ( elementBelowOffset ) evt.offsetY = evt.layerY = (position.top  - elementBelowOffset.top);


		// für pings, die noch cursor statt dem event als argument erwarten
		evt.x = position.left
		evt.y = position.top
	},

	getPointDistance = function( x1, y1, x2, y2 )
	{
		var distance = 0,
			normalize = 100000,
			xDistance,
			yDistance;

		// normalize values
		x1 += normalize;
		y1 += normalize;
		x2 += normalize;
		y2 += normalize;

		// base distance
		xDistance = Math.max( x1, x2 ) - Math.min( x1, x2 );
		yDistance = Math.max( y1, y2 ) - Math.min( y1, y2 );

		if ( xDistance === 0 ) {

			return yDistance;
		}
		else if ( yDistance === 0 ) {

			return xDistance;
		}
		else {

			// pythagoras
			return Math.sqrt( Math.pow( xDistance, 2 ) + Math.pow( yDistance, 2 ) );
		}
	};








	// public stuff

	cursor.update = function ( options, fastMode )
	{
		var lastPointTolerance;

		if ( options )
		{
			settings = $.extend(settings, options);

			// make all event options an array
			for (var event in settings.events)
			{
				if (settings.events.hasOwnProperty(event)) {
					settings.events[event] = getEventsArray(settings.events[event], event);
				}
			}

			cursor.css({
				left: settings.x,
				top: settings.y
			});

			return cursor.animate({
				display:""
			});
		}

		if ( !fastMode )
		{
			windowWidth  = document.body.offsetWidth || document.body.clientWidth || $(document.body).width();
			windowHeight = document.body.offsetHeight || document.body.clientHeight || $(document.body).height();
			scale        = getScale();
			scroll       = typeof window.scrollX == "number" ? {
				x: window.scrollX,
				y: window.scrollY
			} : {
				x: window.pageXOffset,
				y: window.pageYOffset
			}
			lastPointTolerance = 0;
		}
		else
		{
			lastPointTolerance = 5;
		}


		position       = cursor.position();
		positionScaled = position;

		if ( hasTransform(el) )
		{
			positionScaled = cursor._offset();
		}

		cursor.x = position.left;
		cursor.y = position.top;


		elementBelow = getElementAtCurrentPoint( lastPointTolerance );

		return {
			"settings": settings,
			"position": position,
			"positionScaled": positionScaled,
			"elementBelow": elementBelow
		};

	}

	cursor.move = function (options, callbackFN)
	{
		cursor.update();

		var moveTo = {
			x: settings.x + (options.x || 0),
			y: settings.y + (options.y || 0)
		}

		if ( typeof callbackFN == "function" )
		{
			options.complete = callbackFN;
		}

		$.extend(settings, options);

		settings.x = moveTo.x;
		settings.y = moveTo.y;

		cursor.moveTo(settings);
	}

	cursor.moveTo = function (options, callbackFN)
	{
		cursor.update();

		if ( options instanceof jQuery || "nodetype" in options ){
			var $el = $(options);
			if ( $el.length )
			options = {
				x: $el.offset().left + $el.outerWidth(true)*0.5,
				y: $el.offset().top + $el.outerHeight(true)*0.5
			};
			else console.error("invalid arguments");
		}
		if ( typeof callbackFN == "function" )
		{
			options.complete = callbackFN;
		}

		$.extend(settings, options);

		var
		path,
		bezier_params,
		movement = {
			x: settings.x - position.left,
			y: settings.y - position.top
		},
		a1, a2,
		targetAngle,
		rightAngleDeviance,
		curveAngle = [0,0],
		curveLength = 0;


		// wenn jquery.path plugin nicht verfügar, ODER senkrechter winkel, dann standard animation
		if ( !("path" in $) || movement.x === 0 || movement.y === 0)
		{
			return cursor.animate({
				left: settings.x,
				top: settings.y
			},{
				step: onCursorMove,
				queue: false,
				complete: onCursorComplete,
				duration: (Math.abs(movement.x)+Math.abs(movement.y)+settings.duration ) / 2
			});
		}


		targetAngle = Math.atan(movement.x/movement.y) * 180/Math.PI;
		rightAngleDeviance = Math.abs(Math.abs(Math.abs(targetAngle)-45)-45);

		a1 = rightAngleDeviance * 0.66; // at this point moving up/down/left/right will result in an angle of 0 (=no curve) while moving diagonal will result in angles of 30 and 20 (starting point mover and end point mover)
		a2 = rightAngleDeviance * 0.44;
		a1 = (a1 + 30)/2; // we reduce the effect by a factor of 0.5
		a2 = (a2 + 20)/2;

		// kurven properties festlegen
		if ( Math.abs(movement.x) > Math.abs(movement.y) )
		{
			if ( movement.x < 0 )
			{
				curveAngle = [-a1,a2];
			}
			else
			{
				curveAngle = [a1,-a2];
			}
			curveLength = Math.abs(movement.y) / Math.abs(movement.x);
		}
		else
		{
			if ( movement.y < 0 )
			{
				curveAngle = [-a1,a2];
			}
			else
			{
				curveAngle = [a1,-a2];
			}
			curveLength = Math.abs(movement.x) / Math.abs(movement.y);
		}
		curveLength = Math.min(curveLength, 0.5);


		bezier_params = {
			start: {
				x: position.left,
				y: position.top,
				angle: curveAngle[0]
			},
			end: {
				x: settings.x,
				y: settings.y,
				angle: curveAngle[1],
				length: curveLength
			}
		}

		path = new $.path.bezier(bezier_params);

		return cursor.animate({
			path: path
		},{
			step: onCursorMove,
			queue: false,
			complete: onCursorComplete,
			duration: (Math.abs(movement.x)+Math.abs(movement.y)+settings.duration ) / 2 // duration ist 50% settings und 50% durch pfadlänge bestimmt
		});

	}

	// get an event in case you need one :)
	cursor.getEvent = function ( type, appendEvent )
	{
		cursor.update();

		return triggerEvent( type, elementBelow, {appendEvent: appendEvent} );
	}

	cursor.doClick = function ( appendEvent, doMousedown, doMouseup, noAnimation )
	{

		cursor.update();

		for (var i = 0; i < settings.events.click.length; i++)
		{
			lastEvent = triggerEvent(settings.events.click[i], elementBelow, {appendEvent: appendEvent});
		}

		if ( doMousedown ){
			console.log('Deprecated: use cursor.doMousedown()');
			cursor.doMousedown( appendEvent, noAnimation );
		}

		if ( doMouseup ){
			console.log('Deprecated: use cursor.doMouseup()');
			cursor.doMouseup( appendEvent, noAnimation );
		}


		if ( noAnimation ){
			if ( typeof settings.onClick == "function" )
				settings.onClick.call(cursor,lastEvent);
		}
		else {
			cursor.doClickAnimation(function () {
				if ( typeof settings.onClick == "function" )
					settings.onClick.call(cursor,lastEvent);
			});
		}


		return cursor.animate({
			display: ""
		});
	}

	cursor.doClickAnimation = function ( callbackFN )
	{
		setTimeout(function () {
			cursor.doMouseupAnimation();

			if ( typeof callbackFN == "function" )
				callbackFN();
		}, 100);

		return cursor.doMousedownAnimation();
	}

	cursor.doMousedown = function ( appendEvent, noAnimation )
	{

		cursor.update();

		for (var i = settings.events.mousedown.length - 1, evt; i >= 0; i--)
		{
			lastEvent = triggerEvent(settings.events.mousedown[i], elementBelow, {appendEvent: appendEvent});
		}

		if ( noAnimation ){
			if ( typeof settings.onMousedown == "function" )
				settings.onMousedown.call(cursor,lastEvent);
		}
		else {
			cursor.doMousedownAnimation( function () {
				if ( typeof settings.onMousedown == "function" )
				settings.onMousedown.call(cursor,lastEvent);
			});
		}

		return cursor.animate({
			display: ""
		});
	}

	cursor.doMousedownAnimation = function ( callbackFN )
	{
		if ( typeof callbackFN == "function" )
			callbackFN();

		cursor.css({
			marginTop: "2px",
			marginLeft: "1px"
		});

		return cursor.animate({
			display: ""
		});
	}

	cursor.doMouseup = function ( appendEvent, noAnimation )
	{

		cursor.update();

		for (var i = 0; i < settings.events.mouseup.length; i++)
		{
			lastEvent = triggerEvent(settings.events.mouseup[i], elementBelow, {appendEvent: appendEvent});
		}

		if ( noAnimation ){
			if ( typeof settings.onMouseup == "function" )
				settings.onMouseup.call(cursor,lastEvent);
		}
		else {
			cursor.doMouseupAnimation( function () {
				if ( typeof settings.onMouseup == "function" )
					settings.onMouseup.call(cursor,lastEvent);
			});
		}

		return cursor.animate({
			display: ""
		});
	}

	cursor.doMouseupAnimation = function ( callbackFN )
	{
		if ( typeof callbackFN == "function" )
			callbackFN();

		cursor.css({
			marginTop: "",
			marginLeft: ""
		});

		return cursor.animate({
			display: ""
		});
	}

	cursor.doDoubleclick = function ()
	{

		cursor.update();

		for (var i = 0; i < settings.events.dblclick.length; i++)
		{
			lastEvent = triggerEvent(settings.events.dblclick[i], elementBelow);
		}

		cursor.css({
			marginTop: "2px",
			marginLeft: "1px"
		});
		setTimeout(function () {
			cursor.css({
				marginTop: "",
				marginLeft: ""
			});

			if ( typeof settings.onDoubleClick == "function" )
				settings.onDoubleClick.call(cursor,lastEvent);
		},100);

		return cursor.animate({
			display: ""
		});
	}


	init(options);

}
