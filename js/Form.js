var serialize = require("form-serialize");
var Static = require("Static.js");

function Form( element, options ) {

	var api = {
		storeData: storeData,
		restoreData: restoreData,
		clearData: clearData,
		sendData: sendData
	};
	var formElement = null;
	var method = "";
	var action = "";
	var id = "";

	function initialize() {

		if ( element instanceof Element ) {

			options = options || {};

			formElement = element;

			if ( options.ajax ) {
				formElement.addEventListener("submit", onFormSubmit);
			}

			id = formElement.getAttribute("id");
			method = (formElement.getAttribute("method") || "").toLowerCase();
			action = formElement.getAttribute("action");

			return true;
		}
	}

	function sendData( onRequestLoad ) {

		var XHR = new XMLHttpRequest();
		var FD = null;

		switch ( method ) {

			case "post":
				// Bind the FormData object and the form element
				FD = new FormData(formElement);
				break;

			case "get":
				action += action.test(/\?.*=/) ? "&" : "?";
				action += serialize(formElement);
		}

		// Define what happens on successful data submission
		XHR.addEventListener("load", function (event) {
			if ( typeof onRequestLoad == "function" ) {
				onRequestLoad(event);
			}
		});

		// Define what happens in case of error
		XHR.addEventListener("error", function (event) {
			console.error("onRequestError", FD, event);
		});

		// Set up our request
		XHR.open(method, action);


		// The data sent is what the user provided in the form
		XHR.send(FD);
	}

	function onFormSubmit(event) {

		event.preventDefault();
		sendData();
	}

	function storeData( $step ) {

		Static.each(element.querySelectorAll("input, select"), function (item, i) {

			if ( item.getAttribute("type") === "file" ) return;
			if ( item.getAttribute("type") === "radio" && item.checked === false ) return;

			var itemId = item.getAttribute("name");

			if ( item.value ){
				window.sessionStorage.setItem(id +"_"+ itemId, item.value);
			}
		});
	}

	function clearData( clearValues ) {

		Static.each(element.querySelectorAll("input, select"), function (item, i) {

			if ( item.getAttribute("type") === "file" ) return;
			if ( item.getAttribute("type") === "radio" && item.checked === false ) return;

			var itemId = item.getAttribute("name");

			if ( clearValues ) {

				if ( item.getAttribute("type") === "radio" ) {
					item.checked = false;
					item.removeAttribute("checked");
				} else {
					item.value = null;
				}
			}
			window.sessionStorage.removeItem(id +"_"+ itemId);
		});
	}

	function restoreData() {

		Static.each(element.querySelectorAll("input, select"), function (item, i) {

			if ( item.getAttribute("type") === "file" ) return;

			var itemId = item.getAttribute("name");
			var value = window.sessionStorage.getItem(id +"_"+ itemId);

			if ( item.getAttribute("type") === "radio" ) {
				item.checked = true;

			} else if ( value && !item.value ) {
				item.value = value;
			}
		});
	}

	return initialize() && api;
}
if ( typeof module == "object" ) {
	module.exports = Form;
}

