var Static = require("Static.js");

function Select( element ) {

	var api = {
		destroy: destroy,
		getSelectedValues: getSelectedValues,
		getSelectedLabels: getSelectedLabels
	};

	var dropdown = null;
	var popupItems = null;

	var isVisible = null;
	var id = "";
	var placeholder = null;
	var optionElements = [];
	var dropdownPositioning = null;

	var selectMock = null;
	var multiOptionMock = null;

	var classes = {
		POPUP: "popup",
		POPUP_ITEM: "popup__item"
	}

	function initialize() {

		if ( element instanceof HTMLSelectElement && element.parentNode ) {

			optionElements = element.querySelectorAll("option");
			if ( optionElements ) {

				dropdown = document.createElement("div");
				dropdown.classList.add(classes.POPUP);

				setDropdownVisible( false );

				id = element.getAttribute("id") || element.getAttribute("name") || "id" + Static.createHash(element.textContent);
				placeholder = element.getAttribute("placeholder");

				popupItems = Static.map(optionElements, function (option) {

					var popupItem = document.createElement("div");
						popupItem.className = classes.POPUP_ITEM;
						popupItem.setAttribute("data-value", option.value);
						popupItem.textContent = option.textContent;
						popupItem.setAttribute("data-form-id", id);

					dropdown.appendChild(popupItem);

					return popupItem;
				});

				transferSelectedStatusToDropdown();

				document.addEventListener("mousedown", onDocumentMouseDown, true);
				document.addEventListener("keyup", onKeyUp, true);
				document.addEventListener("scroll", onDocumentScroll);

				Static.dom.insertAfter( dropdown, element );


				if ( element.getAttribute("multiple") !== null || placeholder ) {

					selectMock = document.createElement("select");
					selectMock.classList.add("select-mock");
					multiOptionMock = document.createElement("option");
					multiOptionMock.setAttribute("selected", "selected");
					selectMock.appendChild(multiOptionMock);

					Static.dom.insertBefore( selectMock, element );

					element.style.display = "none";
				}

				transferSelectedStatusToDropdown();

				if ( multiOptionMock ) {

					if ( placeholder ) {
						selectMock.setAttribute("placeholder", "");
						multiOptionMock.innerHTML = placeholder;
					} else {
						multiOptionMock.textContent = getSelectedLabels().join(", ");
					}
				}

				requestAnimationFrame(function () {
					dropdownPositioning = getComputedStyle(dropdown).position
					if ( dropdownPositioning == "fixed" ) {
						document.body.appendChild( dropdown );
					}
				});
			}
		}
	}

	function onDocumentScroll() {

		if ( isVisible && dropdownPositioning == "fixed" ) {
			positionDropdown();
		}
	}

	function destroy() {

		element.style.display = "";
		if ( dropdown && dropdown.parentNode ) {
			dropdown.parentNode.removeChild( dropdown );
		}
		if ( selectMock ) {
			selectMock.parentNode.removeChild( selectMock );
		}
		document.removeEventListener("mousedown", onDocumentMouseDown, true);
	}

	function onKeyUp(event) {

		if ( isVisible ) {

			var char = String.fromCharCode(event.keyCode);

			Static.each(popupItems, function (popupItem) {

				if ( popupItem.textContent.indexOf(char) === 0 ) {

					scrollToItem( popupItem );
					return true;
				}
			});
		}
	}

	function scrollToItem( popupItem ) {

		if ( dropdown && popupItem ) {
			dropdown.scrollTop = popupItem.offsetTop;
		}
	}

	function getSelectedLabels() {

		return Static.map((optionElements || []), function (option) {
			if ( option.getAttribute("selected") == "selected" ) {
				return option.textContent;
			}
		}).filter(Boolean); // remove empty items
	}

	function getSelectedValues() {

		return Static.map(optionElements, function (option) {
			if ( option.getAttribute("selected") == "selected" ) {
				return option.value;
			}
		}).filter(Boolean); // remove empty items
	}

	function transferSelectedStatusToDropdown() {

		Static.each(popupItems, function (popupItem) {

			Static.each(optionElements, function (option) {
				if ( option.textContent == popupItem.textContent ) {

					if ( option.getAttribute("selected") == "selected" ) {
						popupItem.classList.add("selected");
					} else {
						popupItem.classList.remove("selected");
					}
				}
			});
		});
	}

	function positionDropdown() {

		var select = (selectMock || element);
		var domRect = select.getBoundingClientRect();

		switch ( dropdownPositioning ) {
			case "fixed":
				dropdown.style.left = select.getBoundingClientRect().left + "px";
				dropdown.style.top = select.getBoundingClientRect().top + "px";
			break;

			case "absolute":
				dropdown.style.left = select.offsetLeft + "px";
				dropdown.style.top = select.offsetTop + "px";
			break;

			default:
			console.warn(".popup should be positioned absolute or fixed")
		}

		dropdown.style.maxHeight = (window.innerHeight - domRect.top) + "px";
	}

	function setDropdownVisible(visible) {

		if ( visible ) {

			positionDropdown();

			dropdown.classList.remove("hidden");
			isVisible = true;
		} else {
			dropdown.classList.add("hidden");
			isVisible = false;
		}
	}

	function onPopupItemSelected(popupItem) {

		var labelsOld = getSelectedLabels();
		var targetValue = popupItem.getAttribute("data-value");

		element.value = null;

		Static.map(optionElements, function (option) {
			if ( option.value == targetValue ) {

				if ( option.getAttribute("selected") ) {
					if ( element.getAttribute("multiple") !== null ) {
						option.removeAttribute("selected");
						option.checked = false;
					} else {
	 					element.value = targetValue;
					}
				} else {
					option.setAttribute("selected", "selected");
					option.checked = true;
 					element.value = targetValue;
				}
			} else if ( element.getAttribute("multiple") === null ) {

				option.removeAttribute("selected");
				option.checked = false;
			}
		});

		var labelsNew = getSelectedLabels();
		if ( labelsNew != labelsOld ) {

			transferSelectedStatusToDropdown();

			if ( multiOptionMock ) {

				if ( !labelsNew.length && placeholder ) {

					multiOptionMock.textContent = placeholder;
					selectMock.setAttribute("placeholder", "");
				} else {
					multiOptionMock.textContent = labelsNew.join(", ");
					selectMock.removeAttribute("placeholder");
				}
			}
			element.dispatchEvent(Static.dom.createEvent({target: element, type: "change"}));
		}
	}

	function onDocumentMouseDown(event) {

		if ( event.target == element || event.target == selectMock ) {

			event.preventDefault();

			setDropdownVisible( !isVisible );

		} else if ( popupItems.indexOf(event.target) != -1 && event.target.getAttribute("data-form-id") === id ) {

			onPopupItemSelected(event.target);

			if ( element.getAttribute("multiple") === null ) {

				setDropdownVisible( false );
			}

		} else if ( event.target != dropdown ) {

			setDropdownVisible( false );
		}
	}

	return initialize(), api;
}
if ( typeof module == "object" ) {
	module.exports = Select;
}
