var Static = require("Static.js");

function TableSort( options ) {

	var api = {
		destroy: destroy
	}

	var element = null;
	var order = null;
	var headCells = null;

	function initialize() {
		element = options.element;
		if ( element ) {

			headCells = element.querySelectorAll("th") || element.querySelectorAll("tbody tr:first-child > td") || element.querySelectorAll("tr:first-child > td");

			Static.each(headCells, function (headCell, i) {
				if ( (options.indexes && options.indexes.indexOf(i) != -1) || !options.indexes ) {

					headCell.addEventListener("click", onCellClick)
					headCell.classList.add("sortable");
				}
			});

		}
	}

	function destroy() {
		Static.each(headCells, function (headCell) {
			headCell.removeEventListener("click", onCellClick)
		});
	}

	function onCellClick( event ) {

		var cell = event.target;
		var rowCells = Static.map(event.target.parentNode.children, function (el) {
			return el;
		});
		var indexX = rowCells.indexOf(cell);
		var cellsOfColumn = element.querySelectorAll("td:nth-of-type("+(indexX+1)+")");
		var columsArray = Static.map(cellsOfColumn, function (el) {
			return el;
		});
		var columnCellsOrdered = columsArray.sort( function (a, b) {
			if ( a.textContent.toUpperCase() < b.textContent.toUpperCase() ) {
				return cell.classList.contains( "sorted-desc" ) ? 1 : -1;
			}
			if ( a.textContent.toUpperCase() > b.textContent.toUpperCase() ) {
				return cell.classList.contains( "sorted-desc" ) ? -1 : 1;
			}
			return 0;
		});

		Static.each(columnCellsOrdered, function (cell) {
			var tr = cell.parentNode;
			var body = tr && tr.parentNode;
			body && body.appendChild(tr);
		});

		if ( cell.classList.contains( "sorted-desc" ) ) {
			cell.classList.add( "sorted-asc" );
			cell.classList.remove( "sorted-desc" );
		} else {
			cell.classList.add( "sorted-desc" );
			cell.classList.remove( "sorted-asc" );
		}

		Static.each(headCells, function (otherCell) {
			if ( otherCell != cell ) {
				otherCell.classList.remove( "sorted-desc" );
				otherCell.classList.remove( "sorted-asc" );
			}
		});
	}

	return initialize(), api;
}
if ( typeof module == "object" ) {
	module.exports = TableSort;
}
